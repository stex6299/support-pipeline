#!/bin/bash

sleepmin=10
webPage="index.html"

while true; do

	# Ottengo la risposta dal server
	response=$(ssh pitonebianco@imole.dyn.uninsubria.it "cd pipeline_docking/scripts/; ./giveMeStatus.sh")
	echo "${response}"

	# Valore numerico della percentuale
	myPercent=$(echo "${response}" | tail -1 | awk '{print $(NF-1)}' )
#	myDate=$(echo "${response}" | head -1  )
	myDate=$(echo "${response}" | awk 'FNR == 1 {print}'  )
	myChiavi=$(echo "${response}" | awk 'FNR == 3 {print $3}' )
	mySerrature=$(echo "${response}" | awk 'FNR == 4 {print $3}' )


	# Sostituisco nell'HTML la percentuale
	sed -i "s/\(value=\"\)[^\"]*\"/\1$myPercent\"/" $webPage

	# Sostituisco l'output del comandp
	# sed -i "s/\(Data: \)[^<]*</\1$myDate</" $webPage
	sed -i "s/\(var myDate = \"\)[^;]*;/\1$myDate\";/" $webPage
	sed -i "s/\(Numero Chiavi: \)[^<]*</\1$myChiavi</" $webPage
	sed -i "s/\(Numero Serrature: \)[^<]*</\1$mySerrature</" $webPage
	# sed -i "s/\(Percentuale: \).*%/\1$myPercent %/" $webPage
	sed -i "s/\(var percentVal = \)[^;]*;/\1$myPercent;/" $webPage

	# Copio la pagina sul sito
	cp $webPage /eos/user/s/scarsi/www/statusPipelineNew.html -v

	sleep ${sleepmin}m
done

