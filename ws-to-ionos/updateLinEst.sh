#!/bin/bash
sourcefile="./linearEst.ipynb"


if [ ! -f $sourcefile ]; then
        echo $sourcefile " does not exists"
        exit 1
fi



sleepmin=10

while true; do
	jupyter nbconvert --execute --to html $sourcefile 
        scp ${sourcefile/.ipynb/.html} pitoneverde@pitoneverde.it:/var/www/html/pipeline/parallel-linearEst.html


	echo -e "\e[32mI will sleep \e[1;33m${sleepmin} minutes\e[0m\n"
	sleep ${sleepmin}m
done



# Useful link
# https://stackoverflow.com/questions/36901154/how-export-a-jupyter-notebook-to-html-from-the-command-line
# https://stackoverflow.com/questions/44099409/nbconvert-exporting-to-another-directory
